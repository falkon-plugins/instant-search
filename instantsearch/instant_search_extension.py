# ============================================================
# Instant Search extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
from PySide2 import QtCore


class InstantSearch(Falkon.PluginInterface, QtCore.QObject):
    tabs = {}

    def init(self, state, settingsPath):
        plugins = Falkon.MainApplication.instance().plugins()
        plugins.mainWindowCreated.connect(self.on_main_window_created)
        plugins.mainWindowDeleted.connect(self.on_main_window_deleted)
        plugins.registerAppEventHandler(Falkon.PluginProxy.KeyPressHandler, self)

        if state == Falkon.PluginInterface.LateInitState:
            for window in Falkon.MainApplication.instance().windows():
                self.on_main_window_created(window)

    def unload(self):
        for window in Falkon.MainApplication.instance().windows():
            self.on_main_window_deleted(window)

    def testPlugin(self):
        return True

    def keyPress(self, objtype, obj, event):
        text = event.text()
        if objtype == Falkon.Qz.ObjectName.ON_WebView:
            if not event.nativeModifiers() == 4 and \
                    event.key() in range(33, 196605):
                tab = obj.webTab()
                win = obj.browserWindow()
                try:
                    sbar = self.tabs[win][tab]
                    visible = sbar.isVisible()
                except(AttributeError, RuntimeError):
                    sbar = None

                if not sbar or not visible:
                    sbar = Falkon.SearchToolBar(obj, parent=tab)
                    layout = tab.layout()
                    layout.insertWidget(layout.count() + 1, sbar)
                    self.tabs[win][tab] = sbar
                sbar.focusSearchLine()
                sbar.setText(text)
                return True
        return False

    def on_tab_changed(self, win):
        tab = win.weView().webTab()
        self.tabs[win].setdefault(tab, None)

    def on_main_window_created(self, win):
        tab = win.weView().webTab()
        self.tabs.setdefault(win, {})
        self.tabs[win].setdefault(tab, None)
        win.tabWidget().changed.connect(lambda: self.on_tab_changed(win))

    def on_main_window_deleted(self, win):
        if win not in self.tabs:
            return
        del self.tabs[win]


Falkon.registerPlugin(InstantSearch())
